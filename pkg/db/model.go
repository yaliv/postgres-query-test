package db

type Test struct {
	ID   uint `gorm:"primaryKey"`
	Name string
}
