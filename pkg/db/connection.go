package db

import (
	"fmt"
	"log"
	"os"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

type ConnConfig struct {
	Host, Port, User, Password string
}

var Client *gorm.DB

func Connect(cfg ConnConfig) error {
	dsn := fmt.Sprint("host=", cfg.Host, " port=", cfg.Port, " user=", cfg.User, " password=", cfg.Password)
	var err error
	Client, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.New(log.New(os.Stdout, "\r\n", log.LstdFlags),
			logger.Config{
				SlowThreshold:             100 * time.Millisecond,
				LogLevel:                  logger.Warn,
				IgnoreRecordNotFoundError: false,
				Colorful:                  false,
			}),
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
	return err
}
