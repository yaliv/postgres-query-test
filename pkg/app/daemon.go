package app

import (
	"encoding/json"
	"log"
	"time"

	"poc-dynamic-secret/pkg/db"
)

func QueryTestTicker() {
	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()

	testQuery()

	for range ticker.C {
		testQuery()
	}
}

func testQuery() {
	var data []db.Test
	if r := db.Client.Find(&data); r.Error != nil {
		log.Println("[read data]", r.Error)
	}
	out, _ := json.MarshalIndent(data, "", "  ")
	log.Printf("\nData: %v\n\n", string(out))
}
