package app

import (
	"log"

	"poc-dynamic-secret/pkg/db"
)

func Bootstrap() {
	// DB migrate.
	if err := db.Client.AutoMigrate(db.Test{}); err != nil {
		log.Fatalln("[db.Client.AutoMigrate]", err)
	}

	// Check if table test is still empty.
	var count int64
	if r := db.Client.Model(&db.Test{}).Count(&count); r.Error != nil {
		log.Fatalln("[count data]", r.Error)
	}
	if count == 0 {
		// Insert some data.
		var data = []db.Test{{Name: "test1"}, {Name: "test2"}}
		if r := db.Client.Create(data); r.Error != nil {
			log.Fatalln("[insert data]", r.Error)
		}
	}
}
