package main

import (
	"log"
	"os"

	"poc-dynamic-secret/pkg/app"
	"poc-dynamic-secret/pkg/db"
)

func main() {
	// DB connect.
	dbCfg := db.ConnConfig{
		Host:     os.Getenv("MDRM_DB_HOST"),
		Port:     os.Getenv("MDRM_DB_PORT"),
		User:     os.Getenv("MDRM_DB_USER"),
		Password: os.Getenv("MDRM_DB_PASSWORD"),
	}
	if err := db.Connect(dbCfg); err != nil {
		log.Fatalln("[db.Connect]", err)
	}

	// Run app.
	app.Bootstrap()
	app.QueryTestTicker()
}
